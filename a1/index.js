const http = require('http');

const port = 8000;

//Mock database
let directory = [
	{
		firstName: "Mary Jane",
        lastName: "Dela Cruz",
        mobileNo: "09123456789",
        email: "mjdelacruz@mail.com",
        password: 123
	},
	{
		firstName: "John",
        lastName: "Doe",
        mobileNo: "09123456789",
        email: "jdoe@mail.com",
        password: 123
	}
]

console.log(typeof directory)


const server = http.createServer((req, res) => {
	if(req.url == '/profile' && req.method == "GET"){
		res.writeHead(200, {'Content-Type': 'application/json'});
		res.write(JSON.stringify(directory));
		res.end()
	}

	
//create users
	if(req.url == '/profile' && req.method == "POST"){

		let reqBody = '';

		req.on('data', function(data){
			reqBody += data;

		});

		req.on('end', function(){
			console.log(typeof reqBody);
			reqBody = JSON.parse(reqBody)

			let user = {
				firstName: reqBody.firstName,
				lastName: reqBody.lastName,
        		mobileNo: reqBody.mobileNo,
        		email: reqBody.email,
        		password: reqBody.password,
			}

			directory.push(user);
			console.log(directory)

			res.writeHead(200, {'Content-Type': 'application/json'});
			res.write(JSON.stringify(user));
			res.end()
		})

	}


});

server.listen(port);

console.log(`Server running at localhost:${port}`);
